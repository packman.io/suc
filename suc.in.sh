#!BASH_PATH_HERE

# See https://the-dam.org/docs/explanations/SecureBash.html
POSIXLY_CORRECT=1
case $SHELLOPTS in
    *privileged*)   ;;
    *)  \exec BASH_PATH_HERE -p "$0" "$@" ;;
esac
\unset -f command builtin unset shopt set unalias
\unset -f read true : exit echo printf
\unalias -a
\shopt -u expand_aliases
set +o posix

USER=$(ID_PATH_HERE -run)  # -run == Real User Name
USER_FG_COLOR=$(( 16#$(\echo "$USER" | MD5_PATH_HERE | HEAD_PATH_HERE -c2) ))  # Arbitrary number in [0:255]
USER_BG_COLOR=$(( ("$USER_FG_COLOR" + 128) % 256 ))  # Most of the time, high enough contrast
MAXWIDTH=80
while IFS= read -r line
do
   echo "$line" | FMT_PATH_HERE -w "$MAXWIDTH" | while IFS= read folded_line
   do
       \printf '\e[38;5;243m%(%FT%T%z)T \e[38;5;%dm\e[48;5;%dm%-9s\e[0m %s\e[0m\n' \
                -1 "$USER_FG_COLOR" "$USER_BG_COLOR" "$USER" "$folded_line" >> /var/lib/suc/"$1"
   done
done
