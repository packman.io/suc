#!/usr/bin/env bash
while true
do
    less -r +F /var/lib/suc/$1
    usuc $1
done
